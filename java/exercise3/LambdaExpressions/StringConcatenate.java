/*
1.Requirements
   - Lambda expression Program with a single method interface to concatenate two strings
2.Entities
   - ConcateTwoStrings
   - SingleMethodInterface (Interface)
3.Function Declaration
   - String string(String string1, String string2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as ConcateTwoStrings with interface as SingleMethodInterface
   2.Inside the declaring the single method with two string parameters
   3.In the class main creating interface object and assigning the lambda expression to return two strings concatenate 
   4.Print statement invoking the interface single method with string values and finally return the value using assigned
interface object lambda expression 
5.Psuedocode:
   interface SingleMethodInterface {
	String string(String string1, String string2);

    public class StringConcatenateDemo {

	   public static void main(String[] args) {
	   
	    SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
	    
	    //display the result
	     System.out.println(singleInterface.string("Hello! ", "This is Menaga "));
 */



package com.java.training.core.lambdaexpressions;

interface SingleMethodInterface {
	String string(String string1, String string2);
}


public class StringConcatenateDemo {

	public static void main(String[] args) {
		
	    SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
				
	    System.out.println(singleInterface.string("Hello! ", "This is Menaga "));

	}

}