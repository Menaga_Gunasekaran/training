/*
Requirement
    Split any random text using any pattern you desire

Entity
    Split

Method Signature
    public static void main (String [] args)

Jobs to be done
    1.Create a string and initialise a text to split.
    2.Create a array to split text to array using split() method with particular value.
    3.Print the splited array of text by for loop.

Pseudo Code
public class Split {  
	public static void main(String args[])  {  
		String text =" "; 
		String[] splitString = text.split("\\s", 4);
		for (int i = 0; i < splitString.length; i++) {
			System.out.println(splitString[i]);
    }
  }
}

*/

package com.java.training.regex;
public class Split {  
	public static void main(String args[]) {  
		String text = "Have a good day"; 
		String[] splitString = text.split("\\s", 4);
		for (int i = 0; i < splitString.length; i++) {
			System.out.println(splitString[i]);
    }
  }
}
