
/*
Requirement
    create a pattern for password which contains
    8 to 15 characters in length
    Must have at least one uppercase letter
    Must have at least one lower case letter
    Must have at least one digit

Entity
    PatternDemo

Method Signature
    public static void main (String [] args)

Jobs to be done
    1.Create A string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
    2.Create a patten and put a patten
     =>^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$
    3.Using Matcher,Check whether the condition is satisfied
    4.If it is true,Print the result

Pseudo Code:
public class PatternDemo {
	public static void main (String [] args) {
		String a = " ";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(a);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      print("Match found");
	    } else {
	      print("Match not found");
	    }
*/
package com.java.training.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternDemo {
	public static void main (String [] args) {
		String a = "R0ckStar";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(a);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      System.out.println("Match found");
	    } else {
	      System.out.println("Match not found");
	    }
	}
}
