Address servlet
1)Requirement:-
  Implement the method doDelete() for address servlet

2)Entity :
  AddressServlet class

3)Function Signature
  void doDelete (HttpServletRequest req, HttpServletResponse res) throws Exception()
 
 Jobs to be Done:-
 1. Get address json from http servlet request body and store it in addressJson of type string.
 2. Parse AddressJson into address of type address;
 3. prepare addressSrevice of type AddressService
 4. Invoke delete method using addressService and pass address as parameter.
 
 pseudo code:-