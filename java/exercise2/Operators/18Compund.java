/*Requirement:TO Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
Entities:ArithmeticDemo(given)
Function Declaration:None
ThingsToDo:1. Analyse the code
		   2. Replace the code.
		   3. Compare the code with the given code and check for eqality.
*/
Answer:
class ArithmeticDemo {

    public static void main (String[] args){
        int result = 3;
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        result %= 7; // result is now 3
        System.out.println(result);

    }
}
The compound assignment operators consist of a binary 
operator and the simple assignment operator. They perform 
the operation of the binary operator on both operands and store
the result of that operation into the left operand, which must
be a modifiable lvalue.