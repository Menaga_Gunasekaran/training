/*Requirement: Is the following interface valid?
    public interface Marker {}
Entity:
Function Declaration:
*/
Answer:
Yes. Methods are not required.
 Empty interfaces can be used as types and to mark classes without 
 requiring any particular method implementations. For an example of a useful empty interface, 
 see java.io.Serializable.