/*Reqirements:To compare the enum values using equal method and == operator
Entities:None
Function Declaration :None
ThingsToDo:1.Enum methods have been compared.
			2.Use == operator and equal method for comparision.
*/
 Answer:
1. == operator never throws NullPointerException whereas .equals() method can throw NullPointerException.
2.== is responsible for type compatibility check at compile time whereas .equals() method will never worry about the types of both the arguments.
3. Example:
enum Color { BLACK, WHITE };
Color nothing = null;
if (nothing == Color.BLACK); // runs fine
if (nothing.equals(Color.BLACK)); // throws NullPointerException
== is subject to type compatibility check at compile time

enum Color { BLACK, WHITE };
enum Chiral { LEFT, RIGHT };

if (Color.BLACK.equals(Chiral.LEFT)); // compiles fine
if (Color.BLACK == Chiral.LEFT); // DOESN'T COMPILE!!! Incompatible types!


 