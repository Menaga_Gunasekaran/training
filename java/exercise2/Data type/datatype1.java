/*Requirement:
     To write a program to store and retrieve four boolean flags using a single int

Entity
  Class named BooleanInverter is used.

Jobs to be Done
     1.Inverting the Boolean using the ! Symbol
     2.Converting the true value of the Boolean to false using the ! Symbol..

Solution:
   1.We use ! Symbol to Invert the Boolean. */


public class BooleanInverter {
    public static void main(String[] args) {
        
        boolean flag = false;
        String res = "Fail";
        if(res == "Pass") {
            System.out.println(flag);
        } else {
            flag = !flag;
            System.out.println(flag);
        }
    }
}