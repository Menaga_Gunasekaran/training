/*Requirements: To print fibinocci using for loop, while loop and recursion.
Entities:Fibonacci
Function Declaration:sc 
ThingsToDO:For loop:
					1. Understand the concept of fibonacci series.
					2. Get the number from the user.
					3. Using the for,while,recursion and print the number1,number2.
					4. Add the two numbers into a variable called sum.
					5. Then print the number.
					6.Then assign the values of number2 in number1 and number2 as sum.
					7.Repeat the loop until it satisfies the condition in for loop,while loop,recursion.  
*/
Answer:
For:
public class Fibonacci {
public static void main(String[] args) {
  Scanner sc=new Scanner(System.in);
  int n,i,sum,num1=0,num2=1;
  System.out.println("Enter the maximum number");
  n=sc.nextInt();
  for(i=1;i<=n;i++) {
  
  System.out.println(num1+" ");
  sum=num1+num2;
  num1=num2;
  num2=sum;
  }
 }
 }
While:
public class Fibonacci{
   public static void main(String args[]) {
      int a, b, c, i = 1, n;
      n = 8;
      a = 0;
      b = 1;System.out.print(a+" "+b);

      while(i<n) {
         c = a + b; System.out.print(" ");
         System.out.print(c);
         a = b;
         b = c;
         i++;
      }
   }
}
Recursion:
public class Fibonacci{  
 static int n1=0,n2=1,n3=0;    
 static void printFibonacci(int count){    
    if(count>0){    
         n3 = n1 + n2;    
         n1 = n2;    
         n2 = n3;    
         System.out.print(" "+n3);   
         printFibonacci(count-1);    
     }    
 }    
 public static void main(String args[]){    
  int count=10;    
  System.out.print(n1+" "+n2);//printing 0 and 1    
  printFibonacci(count-2);//n-2 because 2 numbers are already printed   
 }  
}  
