 Requirements:Consider the following code snippet.

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        - What output do you think the code will produce if aNumber is 3?

        Write a test program containing the previous code snippet
        - and make aNumber 3. What is the output of the program?
        - Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        - Use braces, { and }, to further clarify the code.
Entities:None
Function Declaration:None
JobsTobeDone:a)1. Check for the condition.
			   2. Print the result.
			 b)1. Understand the question.
			   2. Analyse it.
			   3. Give the explanation.
			 c)1. Since the value is 3 which is greater than 0 using if condition.
			   2. if value is equal to 0 print first string else second string.
			   3. Third string is common to nested if condition.
			 d) 1. Use of spaces ,lie breaks and braces have been done for better understandings.
Answer:
a)		second string
		third string
		

b)		3 is greater than or equal to 0, so execution progresses to the second if statement.
 The second if statement's test fails because 3 is not equal to 0. Thus, the else clause 
 executes (since it's attached to the second if statement). Thus, second string is displayed. 
 The final println is completely outside of any if statement, 
 so it always gets executed, and thus third string is always displayed.

 
c)		if (aNumber >= 0)
    if (aNumber == 0)
        System.out.println("first string");
    else
        System.out.println("second string");

System.out.println("third string");



d)if (aNumber >= 0) {
    if (aNumber == 0) {
        System.out.println("first string");
    } else {
        System.out.println("second string");
    }
}

System.out.println("third string");