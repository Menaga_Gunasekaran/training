/*Requirements:To Find what's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
Entities:SomethingIsWrong(given)
Function Declaration:none
ThingsToDO:1. Create an object (myReact).
		   2. Print the  output.
		   3. The output will be excecuted without any errors.

Answer:
Explanation:
The code never creates a Rectangle object. With this simple program,
the compiler generates an error. However, in a more realistic situation,
myRect might be initialized to null in one place, say in a constructor, 
and used later. In that case, the program will compile just fine,
but will generate a NullPointerException at runtime.
*/
Fixed Code:
public class Rectangle {
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}
