/*Requirements:To Find what is the output from the following code:
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
Entitites:IdentifyMyParts(given)
Function Declaration:none
JobsToBeDone:1.Finding the output and giving the answers.
			 2.Because x is defined as a public static int
			 in the class IdentifyMyParts, every reference 
			 to x will have the value that was last assigned
			 because x is a static variable (and therefore a 
			 class variable) shared across all instances of 
			 the class. 
			 3.That is, there is only one x: when 
			 the value of x changes in any instance it affects 
			 the value of x for all instances of IdentifyMyParts.
*/
Answers:
 a.y = 5 
 b.y = 6 
 a.x = 2 
 b.x = 2
 IdentifyMyParts.x = 2
