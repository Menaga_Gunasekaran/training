ObjectsAndClasses:
/*Requirement:Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?
	Explain:Class variables,instance variables and mention them.
Entitites:IdentifyMyParts(given)
Function Declaration:None
JobsToBeDone:1.Class and instance variables have been defined/explained.
			 2.Mentioned those variables from the given code.
*/
Answer:
Class Variable   : A class variable is any variable declared with the static 
				  modifier of which a single copy exists, regardless of how
				  many instances of the class exist. Note that only Class 
				  Variable exist in  a class.
Here "x" is the class variable.

Instance Variable:Instance variables are created when an object is created with
				  the use of the keyword 'new' and destroyed when the object is destroyed. 
				  Instance variables hold values that must be referenced by more than one 
				  method, constructor or block, or essential parts of an object's state 
				  that must be present throughout the class.
Here "y" is the instance variable.