/*Requirements:Given the following class, called NumberHolder,
    To Find ,Some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
Entities:NumberHolder(given)
Function Declaration:none
ThingsToDo:1.create a public class.
		   2.Inside void main,assign values to anInt,aFloat.
		   3.Using system.println display the values of anInt,aFloat.
*/
Answer:
	public class NumberHolder {
	  public static void main(String[] args) {
	    NumberHolder aNumberHolder = new NumberHolder();
	    aNumberHolder.anInt = 4;
		aNumberHolder.aFloat = 6.2f;
		System.out.println(aNumberHolder.anInt);
		System.out.println(aNumberHolder.aFloat);
    }
}