/*
Entities:
    Dog.

Function Declaration:
    public void sound().

Jobs to be done:
    1. Declare the class Dog.
    2. call the method sound().
    3. Print the the statement as given.
*/

public class Dog extends Animal {
    public void sound() {
        System.out.println("the dog barks");
    }
}