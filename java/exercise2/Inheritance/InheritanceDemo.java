/*Requirements:demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
Entity:InheritanceDemo
Function Declaration:dog,cat,snake,Animal
JobsToBeDone:1. Create a public class.
             2. Inside main class,create objects like dog,cat,animal,snake.
			 3. print the outputs for run and sound.
*/public class InheritanceDemo{
    public static void main(String[] args){
        Animal animal = new Animal();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        animal.sound();
        animal.run();
        animal.run(7);
        cat.sound();
        cat.run();
        cat.run(6);
        dog.sound();
        dog.run();
        dog.run(8);
        snake.sound();
        snake.run();
        snake.run(9);
    }
}