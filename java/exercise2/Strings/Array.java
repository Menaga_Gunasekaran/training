/*Requirements:The following code creates one array and one string object. How many references to those objects exist after the code executes? Is either object eligible for garbage collection?

...
String[] students = new String[10];
String studentName = "Peter Smith";
students[0] = studentName;
studentName = null;
...

Entities:None
Function Declaration:None
*/
 There is one reference to the students array and that array has one reference to 
 the string Peter Smith. Neither object is eligible for garbage collection. 
 The array students is not eligible for garbage collection because it has one reference 
 to the object studentName even though that object has been assigned the value null. 
 The object studentName is not eligible either because students[0] still refers to it.