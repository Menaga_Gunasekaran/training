/*Requirements:Consider the following string:

String hannah = "Did Hannah see bees? Hannah did.";
What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
Entitiy:None
Function declaration:None
*/
Question: What is the value displayed by the expression hannah.length()?

Answer: 32.

Question: What is the value returned by the method call hannah.charAt(12)?

Answer: e.

Question: Write an expression that refers to the letter b in the string referred to by hannah.

Answer: hannah.charAt(15).


