/*Requirements:Write a program that computes your initials from your full name and displays them.
Entity:ComputeInitials
Function Declaration:myInitials
JobsToBeDone:1.Create a public class ComputeInitials.
             2.Inside main class,create an object myInitials
			 3.Using,for loop and if condition:print the initials .
*/
Answer:
public class ComputeInitials {
    public static void main(String[] args) {
        String myName = "Fred F. Flintstone";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}