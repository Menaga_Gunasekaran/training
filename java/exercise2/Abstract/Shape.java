/*Requirement: Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
Entity:Shape
Function Declaration:None
JobToBeDone:1.Create an abstract class shape and declare a function named draw.
			2.Create a class Rectangle and inherit shape.
			3.Create a class Circle1 and inherit shape.
			4.Create a main class TestAbstraction1 and create an object for shape.
*/
Answer:
abstract class Shape{  
abstract void draw();  
}  

class Rectangle extends Shape{  
void draw(){System.out.println("drawing rectangle");}  
}  
class Circle1 extends Shape{  
void draw(){System.out.println("drawing circle");}  
}  

class TestAbstraction1{  
public static void main(String args[]){  
Shape s=new Circle1();
s.draw();  
}  
}