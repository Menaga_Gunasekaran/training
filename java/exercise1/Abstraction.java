public class Abstraction
{
    public static void main(String args[])
    {
        Person p = new Person()
        { 
            void eat()
            {
                console.writeline("sooper..");
            } 
        }; 
        p.eat();
    }
}
abstract class Person
{ 
    abstract void eat(); 
} 

/*Because it will produce an error.Abstraction is used for hiding information and not creating any new objects*/