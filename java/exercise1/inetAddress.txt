Inet Address:
	-encapsulation of ipaddress
	-hostname and address
	
Methods:
		-getLocalHost()
		-getByName()
		-getAllByName()
		-getHostAddress()
		-getHostName()
		
sub Classes:
machines connected to a network.
Inet4Address for IPv4 addresses-32bit
Inet6Address for IPv6 addresses-128 bit

address types: 
		-Unicast(ip packets single)
		-Multicast.(many )
Inet as instance in socket,serversocket and udp datagram:
An InetAddress corresponds to the Network Layer (Layer 3) and is basically an IP address.

A InetSocketAddress corresponds to the Transport Layer (Layer 4) and consists of an IP address and a port number.