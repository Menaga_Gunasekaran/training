import java.net.*;
public class Demo{
   public static void main(String[] args){
      try{
         InetAddress my_address = InetAddress.getLocalHost();
		 System.out.println("LocalHost is : " + my_address);
         System.out.println("The IP address is : " + my_address.getHostAddress());
         System.out.println("The host name is : " + my_address.getHostName());
		 
		 my_address = InetAddress.getByName("google.com.sa");
		 System.out.println("Google inetaddress is : "+ my_address);
      }
      catch (UnknownHostException e){
         System.out.println( "Couldn't find the local address.");
      }
   }
}