*JAR File:
	-java ARchive.
	-many Java class files and associated metadata and resources (text, images, etc). 
	-compressed form of compiled programs.
	-exported and imported.
	-zip files
	-no IDE.
	-storage and download.
	
*URL COnnection:
	-links user and websites.
	-subclasses
		1.http
		2.JAR
	
*Example:
	String urlString = "http://butterfly.jenkov.com/"
                 + "container/download/"
                 + "jenkov-butterfly-container-2.9.9-beta.jar";

URL jarUrl = new URL(urlString);
JarURLConnection connection = new JarURLConnection(jarUrl);

Manifest manifest = connection.getManifest();


JarFile jarFile = connection.getJarFile();

Jar file in cmd:
https://www.youtube.com/watch?v=wdq4MJm3YWI



	
	