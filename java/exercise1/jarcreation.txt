*JAR in command prompt:
manifest file:
		Manifest-Version: 1.0
		Main-Class:Main class file name
create archive:
		jar cmf FileName.mf FileName.jar FileName.class FileName.java
and run it by typing:
		java -jar FileName.jar
The file  can now be downloaded and executed.
Creating an executable JAR file. Here is the general procedure for creating an executable JAR:

	1.Compile your java code, generating all of the program's class files.
	2.Create a manifest file containing the following 2 lines:
		Manifest-Version: 1.0
		Main-Class: name of class containing main
		
	3.To create the JAR, type the following command:
		jar cmf manifest-file jar-file input-files
	4.To view the contents of the JAR, type:
		jar tf jar-file
	5.Execute the application from the command line by typing:
		java -jar jar-file
		
