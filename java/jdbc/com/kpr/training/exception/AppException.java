/*
Requirements:
    - AppException class
    
Entities:
    - AppException
    
Function Declaration:
    - public AppException()
    - public AppException(ErrorCode error)
    
Job to be done:
    - Create a class ApException extends RuntimeException
    - Declare a constructor with empty parameters and invoke super() method
    - Declare another constructor with parameter - ErrorCode error
    - Get the detail of the error with getDetail() method of enum class
    - Invoke super() method to invoke the properties of Exception
    
Pseudocode:
public class AppException extends RuntimeException {
	
	public AppException() {
		super();
	}

    public AppException(ErrorCode error) {
    	super(error.getDetail());
	}
}
 
 
 * */



package com.kpr.training.exception;

public class AppException extends RuntimeException {
	
	public AppException() {
		super();
	}

    public AppException(ErrorCode error) {
    	super(error.getDetail());
	}
}


