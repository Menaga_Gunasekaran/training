/*
Requirements:
    - Create a enum class with errorcodes
    
Entities:
    - ErrorCode
    
Function Declaration:
    - ErrorCode(String detail)
    - public String getDetail()
    
Job to be done:
    1) Create a enum block
    2) Declare enum constant with value in argument
    3) Declare a string - detail;
    4) Declare a Constructor - ErrorCode(String detail)
    5) Get the value from parameter and set it to the enum constant
    6) Declare a method - getDetail();
    7) Return the detail.
  
 
 * */


package com.kpr.training.exception;

public enum ErrorCode {
	
	ERR01("Mysql Driver Class is not found"),
	ERR02("Property File is not found"),
	ERR03("Error occured in I/O operation of property file"),
	ERR04("Error occured in SQL operation"),
	ERR05("Something went wrong"),
	ERR06("Query is not executes successfully"),
	ERR07("No data found"),
	ERR08(""),
	ERR09(""),
	ERR10(""),
	ERR11(""),
	ERR12(""),
	ERR13(""),
	ERR14(""),
	ERR15(""),
	ERR16(""),
	ERR17(""),
	ERR18(""),
	ERR19(""),
	ERR20(""),
	ERR21(""),
	ERR22(""),
	ERR23(""),
	ERR24(""),
	ERR25(""),
	ERR26(""),
	ERR27(""),
	ERR28(""),
	ERR29(""),
	ERR30("");

	String detail;
	
	ErrorCode(String detail) {
		this.detail = detail;
	}
	
	public String getDetail() {
		return this.detail;
	}
}
