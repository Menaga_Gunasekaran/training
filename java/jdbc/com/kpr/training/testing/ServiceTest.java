/*
Requirements:
    - Write test cases for Person and Address class
    
Entities:
    - ServiceTest
    
Function Declaration:
    - public void insertAddress()
    - public void insertPersonAddress()
    
Job to be done:
    - Create a class ServiceTest
    - Declare and define insertAddress() method
    - Create a Address and AddressService object
    - Invoke create() method to create the address in db
    - Invoke  assertTrue() method and give the appropriate condition
    
    
    
 * 
 * 
 * */


package com.kpriet.training.jdbc.testing;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kpr.training.model.Address;
import com.kpr.training.model.Person;
import com.kpr.training.service.AddressServiceDemo;
import com.kpr.training.service.PersonServiceDemo;

public class ServiceTest {

	@Test(priority = 1, description = "Insert Address")
	public void insertAddress() {
		Address addressOne = new Address("Townhall", "Coimbatore", 641146);
		AddressServiceDemo service = new AddressServiceDemo();
		Address storedAddress = service.create(addressOne);
		System.out.println("Address Stored in id - " + storedAddress.getId());
		Assert.assertTrue(storedAddress.getId() != 0L);
	}
	
	@Test(priority = 2, description = "Insert Person and address")
	public void insertPersonAddress() {
		PersonServiceDemo personService = new PersonServiceDemo();
		AddressServiceDemo addressService = new AddressServiceDemo();
		Address address = new Address("Busstand", "Covai", 654321);
		Person person = new Person("Revathi", "Revathi@gmail.com", "1975-02-15");
		Person storedPerson = personService.create(person, address);
		Address storedAddress = addressService.read(storedPerson.getAddressId());
		System.out.println("Stored Values : ");
		System.out.println(storedPerson.getId() + " - " +  storedPerson.getName() + " - " + storedPerson.getBirthDate());
		System.out.println(storedAddress.getStreet() + " - " + storedAddress.getCity() + " - " + storedAddress.getPincode());
		Assert.assertTrue(person.getId() != 0L && address.getId() != 0L);
	}
}
