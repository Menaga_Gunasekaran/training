/*
Requirements:
    - Create a class with methods to connect with database
    
Entities:
    - ConnectionService
    
Function Declaration:
    - public Connection getConnection()

Job to be done:
    1) Create class ConnectionService
    
    //get() method - public Connection get()
    1) Invoke Class.forName("com.mysql.cj.jdbc.Driver") method to run all its static method
    2) Create a Properties object - dbDetails
    3) Create a InputStream object - reader to read "db.properties" file
    4) Invoke load() method to load the contents from db.properties file to dbDetails
    5) Get the values of url, username and password from dbDetails with getProperty(key) method
    6) Invoke DriverManager.getConnection(url, username, password) and store the returned value in connection of type Connection
    7) Return Connection object - connection
    
    //init() method - public void init(Connection connection)
    1) Invoke connection.setAutoCommit(false) method
    
    //commit() method - public void commit(Connection connection)
    1) Invoke connection.commit() methos
    
    //release method - public void release(Connection connection)
    1) Invoke connection.close() method
    
Pseudo code:
public class ConnectionService {
    
    public void get() {
        
        Class.forName("com.mysql.cj.jdbc.Driver");
        Properties dbDetails = new Properties();
        InputStream reader = new FileInputStream("db.properties");
        dbDetails.load(reader);
        Connection connection = DriverManager.getConnection(dbDetails.getProperty("URL"),
                                                            dbDetails.getProperty("USERNAME"),
                                                            dbDetails.getProperty("PASSWORD"));
        return connection;
    }
    
    public void init(Connection connection) {
        connection.setAutoCommit(false);
    }
    
    public void commit(Connection connection) {
        connection.commit();
    }
    
    public void release(Connection connection) {
        connection.close();
    }
    
}
 
 * */


package com.kpr.training.connection;

import java.io.FileInputStream;

import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCode;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionService {

    public Connection get() {
        
    	Connection connection = null;
        try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Properties dbDetails = new Properties();
	        InputStream reader = new FileInputStream("db.properties");
	        dbDetails.load(reader);
	        connection = DriverManager.getConnection(dbDetails.getProperty("URL"),
	                                                 dbDetails.getProperty("USERNAME"),
	                                                 dbDetails.getProperty("PASSWORD"));
		} catch (ClassNotFoundException e) {
			throw new AppException(ErrorCode.ERR01);
		} catch (FileNotFoundException e) {
			throw new AppException(ErrorCode.ERR02);
		} catch (IOException e) {
			throw new AppException(ErrorCode.ERR03);
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR04);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05);
		}
        return connection;
    }
    
    public void init(Connection connection) {
        try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR04);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05);
		}
    }
    
    public void commit(Connection connection) {
        try {
			connection.commit();
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR04);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05);
		}
    }
    
    public void release(Connection connection) {
        try {
			connection.close();
		} catch (SQLException e) {
			throw new AppException(ErrorCode.ERR04);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR05);
		}
    }
    
}
