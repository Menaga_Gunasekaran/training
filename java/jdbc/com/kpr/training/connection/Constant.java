package com.kpr.training.connection;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constant {
	public static String URL;
	public static String USERNAME; 
    public static String PASSWORD;
    
    public static void propReader() { 
    	Properties file = new Properties();
    	try {
			InputStream reader = new FileInputStream("db.properties");
			file.load(reader);
			URL = file.getProperty("URL");
			USERNAME = file.getProperty("USERNAME");
			PASSWORD = file.getProperty("PASSWORD");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
