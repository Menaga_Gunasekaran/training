
package com.kpr.training.connection;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.util.List;
import java.util.Properties;

import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;
import com.kpr.training.service.AddressService;
import com.kpr.training.service.PersonService;


public class Check {

	public static void main(String[] args) throws AppException {
		/*-------------Create Address-----------*/
		//Address addressOne = new Address("Townhall", "Coimbatore", 641146);
		//AddressService service = new AddressService();
		//Address storedAddress = service.create(addressOne);
		//System.out.println("Address Stored in id - " + storedAddress.getId());
		
		
		//Address addressOne = new Address("Townhall", "Coimbatore");
		//AddressService service = new AddressService();
		//Address storedAddress = service.create(addressOne);
		//System.out.println("Address Stored in id - " + storedAddress.getId());
		
		
		/*-------------Read Address-------------*/
		/*AddressService service = new AddressService();
		Address address = service.read(20L);
		System.out.println("Address of id 19L : " + address.getStreet() + " - " + address.getCity() + " - " + address.getPincode());
		*/
		
		/*------------Read All Address-------------*/
		//AddressService service = new AddressService();
		//List<Address> addresses = service.readAll();
		//for(Address address : addresses) {
		//	System.out.println(address.getId() + " - " + address.getStreet() + " - " + address.getCity() + " - " + address.getPincode());
		//}
		
		
		
		/*-------------Update Address-----------*/
//		AddressService addressService = new AddressService();
//		Address address = addressService.read(19L);
//		System.out.println("Address of id 19L : " + address.getStreet() + " - " + address.getCity() + " - " + address.getPincode());
//		address.setStreet("Gandhipuram");
//		address.setCity("Covai");
//		address.setPincode(656565);
//		address = addressService.update(address);
//		System.out.println("Updated Address of id 19L : " + address.getStreet() + " - " + address.getCity() + " - " + address.getPincode());
		
		
		/*-------------Delete address-------------*/
		//AddressService addressService = new AddressService();
		//addressService.delete(19l);
		
		
		
		/*-------------Create Person------------*/
		//Person personOne = new Person("Sarath", "sarath01@gmail.com", 2L, "2001-01-08");
		//PersonService personService = new PersonService();
		//Person storedPerson = personService.create(personOne);
		//System.out.println("Person is stored in id - " + storedPerson.getId());
		
		
		/*------------Create Person and Address ----------------*/
		/*
		PersonService personService = new PersonService();
		AddressService addressService = new AddressService();
		Address address = new Address("Busstand", "Covai", 654321);
		Person person = new Person("Revathi", "Revathi@gmail.com", "1975-02-15");
		Person storedPerson = personService.create(person, address);
		Address storedAddress = addressService.read(storedPerson.getAddressId());
		System.out.println("Stored Values : ");
		System.out.println(storedPerson.getId() + " - " +  storedPerson.getName() + " - " + storedPerson.getBirthDate());
		System.out.println(storedAddress.getStreet() + " - " + storedAddress.getCity() + " - " + storedAddress.getPincode());
		*/
		
		
        /*------------Read Person with and without address------------*/
		/*
		PersonService personService = new PersonService();
		Person person = personService.read(17L, true);
		person = personService.read(17L, false);
		*/
		
		/*------------Read All person--------------*/
		/*
		PersonService personService = new PersonService();
		List<Person> persons = personService.readAll();
		for (Person person : persons) {
			System.out.println(person.getName() + " - " + person.getEmail());
		}
		*/
		
		/*------------Update Person-----------*/
		/*
		PersonService personService = new PersonService();
		Person person = personService.read(17L, false);
		person.setBirthDate("1970-01-10");
		person = personService.update(person);
		System.out.println("Person Updated : " + person.getBirthDate());
		*/
		
		
		/*-----------Delete Person-------------*/
		/*
		PersonService personService = new PersonService();
		if (personService.delete(16L) == true) {
			System.out.println("Person with id - 16  is deleted");
		}
		*/
		
		/*
		Properties file = new Properties();
		OutputStream writer = null;
		try {
			writer = new FileOutputStream("db.properties");
			file.setProperty("URL", "jdbc:mysql://localhost:3306/jdbc_demo");
			file.setProperty("USERNAME", "root");
			file.setProperty("PASSWORD", "guru.GP1");
			file.store(writer, null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		/*
		Properties file = new Properties();
		OutputStream writer = null;
		try {
			writer = new FileOutputStream("db.properties");
			file.setProperty("1", "Class is not found");
			file.store(writer, null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}
