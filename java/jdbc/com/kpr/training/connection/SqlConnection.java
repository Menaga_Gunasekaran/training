package com.kpr.training.connection;

import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;

public class SqlConnection {
	public Connection connect = null;
	public PreparedStatement statement = null;

	public SqlConnection connect(String query) {
		SqlConnection connector = new SqlConnection();
		try {
			Constant.propReader();
			Class.forName("com.mysql.cj.jdbc.Driver");
			connector.connect = DriverManager.getConnection(Constant.URL, Constant.USERNAME, Constant.PASSWORD);
			connector.statement = connector.connect.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		} catch (ClassNotFoundException e) {
			throw new AppException(e.getMessage(), "5");
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "1");
		}
		return connector;
	}
	
	
	public boolean formatCheck(Person person) {
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
		Matcher matcher = pattern.matcher(person.getEmail());
		if (matcher.matches()) {
			pattern = Pattern.compile("\\d{4}-[01]\\d-[0-3]\\d");
			matcher = pattern.matcher(person.getBirthDate());
			if (matcher.matches()) {
				if (person.getName() != null & person.getName() != "") {
						return true;
				} else {
					throw new AppException("Enter value for name", "5");
				}
			} else {
				throw new AppException("Enter date in yyyy-mm-dd format", "5");
			}
		} else {
			throw new AppException("Enter proper email", "5");
		}
	}
	
	public boolean formatCheck(Address address) {
		if (address.getStreet() != null & address.getStreet() != "" ) {
			if (address.getCity() != null & address.getCity() != "") {
				if (address.getPincode() != 0) {
					return true;
				} else {
					throw new AppException("Enter some value in Pincode", "5");
				}
			} else {
				throw new AppException("Enter some value in City", "5");
			}
		} else {
			throw new AppException("Enter some value in Street", "5");
		}
	}
	
	public String getAddressInsert() {
		return "INSERT INTO Address(street, city, pincode) \r\n" + 
				"    VALUES (?, ?, ?)";
	}
	
	public String getAddressReader() {
		return "SELECT id, street, city, pincode FROM ADDRESS";
	}
	
	public String getAddressSingleReader() {
		return "SELECT id, street, city, pincode FROM ADDRESS" + 
				"    WHERE id = ?;";
	}
	
	public String getAddressUpdate() {
		return "UPDATE Address" + 
				"    SET street=?" + 
				"        ,city=?" + 
				"        ,pincode=?" + 
				"	    WHERE id=?";
	}
	
	public String getAddressDelete() {
		return "DELETE FROM Address" + 
				"    WHERE id = ?; ";
	}
	
	public String getPersonInsert() {
		return "INSERT INTO Person(name, email, address_id, birth_date)" + 
				"    VALUES (?, ?, ?, ?);";
	}
	
	public String getPersonRead() {
		return "SELECT id, name, email, address_id, birth_date, created_date " + 
				"    FROM Person" + 
				"        WHERE id = ?;";
	}
	
	public String getPersonReadAll() {
		return "SELECT id, name, email, address_id, birth_date, created_date " + 
				"    FROM Person";
	}
	
	public String getPersonUpdate() {
		return "UPDATE Person" + 
				"    SET name = ? " + 
				"        ,email = ? " + 
				"        ,address_id = ? " + 
				"        ,birth_date = ? " + 
				"            WHERE id = ?;";
	}
	
	public String getPersonDelete() {
		return "DELETE FROM Person" + 
				"    WHERE id = ?;";
	}
}
