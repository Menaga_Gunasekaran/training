/*
Requirements:
    - Person pojo class 
    
Entities:
    - Person

Function Declaration:
    - public Person (Long id, String name, String email, Long address_id, String birth_date)
    - public Person (Long id, String name, String email, Long address_id, String birth_date, String created_date)
    - public Person (String name, String email, Long address_id, String birth_date)
    - public Person (String name, String email, String birth_date)
    - public Long getId()
    - public String getName()
    - public String getEmail()
    - public Long getAddressId()
    - public String getBirthDate()
    - public String getCreatedDate()
    - public void setId(Long id)
    - public void setName(String name)
    - public void setEmail(String email)
    - public void setAddressId(Long address_id) 
    - public void setBirthDate(String birthDate)
    - public void setCreatedDate(String createdDate) 
    
Job to be done:
    1) Create class Person
    2) Declare properties - id, name email, address_id, birth_date, created_date
    3) Declare a constructor class with parameter - id, name, email, address_id, birth_date
        3.1) Get the values and set them to properties
    4) Declare a constructor class with parameter - id, name, email, address_id, birth_date, created_date
        4.1) Get the values and set them to properties
    5) Declare a constructor class with parameter -  name, email, address_id, birth_date
        5.1) Get the values and set them to properties
    6) Declare a getId() method to return id
    7) Declare a getName() method to return name
    8) Declare a getEmail() method to return email
    9) Declare a getAddressId() method to return address_id
    10) Declare a getBirthDate() method to return birth_date()
    11) Declare a getCreatedDate() method to return created_date
    12) Declare a setId(Long id) method to get id from parameter and set it to property
    13) Declare a setName(String name) to get name from parameter and set it to property
    14) Declare a setEmail(String email) to get ema8il from parameter and set it to property
    15) Declare a setAddressId(Long address_id) to get address_id from parameter and set it to property
    16) Declare a setBirthDate(String birth_date) to get birth_dat from parameter and set it to property
    17) Declare a setCreatedDate(String created_date) to get created_date from parameter and set it to property
    
Pseudo code:

public class Person {

	private Long id;
	private String name;
	private String email;
	private Long address_id;
	private String birth_date;
	private String created_date;
	
	public Person (Long id, String name, String email, Long address_id, String birth_date) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.address_id = address_id;
		this.birth_date = birth_date;
	}
	
	public Person (Long id, String name, String email, Long address_id, String birth_date, String created_date) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.address_id = address_id;
		this.birth_date = birth_date;
		this.created_date = created_date;
	}
	
	public Person (String name, String email, Long address_id, String birth_date) {
		this.name = name;
		this.email = email;
		this.address_id = address_id;
		this.birth_date = birth_date;
	}
	
	public Person (String name, String email, String birth_date) {
		this.name = name;
		this.email = email;
		this.birth_date = birth_date;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public Long getAddressId() {
		return this.address_id;
	}
	
	public String getBirthDate() {
		return this.birth_date;
	}
	
	public String getCreatedDate() {
		return this.created_date;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setAddressId(Long address_id) {
		this.address_id = address_id;
	}
	
	public void setBirthDate(String birthDate) {
		this.birth_date = birthDate;
	}
	
	public void setCreatedDate(String createdDate) {
		this.created_date = createdDate;
	}
}

 
 
 * */


package com.kpr.training.model;

public class Person {

	private Long id;
	private String name;
	private String email;
	private Address address;
	private String birth_date;
	private String created_date;
	
	public Person (Long id, String name, String email, Address address, String birth_date) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.address = address;
		this.birth_date = birth_date;
	}
	
	public Person (Long id, String name, String email, Address address, String birth_date, String created_date) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.address = address;
		this.birth_date = birth_date;
		this.created_date = created_date;
	}
	
	public Person (String name, String email, Address address, String birth_date) {
		this.name = name;
		this.email = email;
		this.address = address;
		this.birth_date = birth_date;
	}
	
	public Person (String name, String email, String birth_date) {
		this.name = name;
		this.email = email;
		this.birth_date = birth_date;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public Address getAddress() {
		return this.address;
	}
	
	public String getBirthDate() {
		return this.birth_date;
	}
	
	public String getCreatedDate() {
		return this.created_date;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public void setBirthDate(String birthDate) {
		this.birth_date = birthDate;
	}
	
	public void setCreatedDate(String createdDate) {
		this.created_date = createdDate;
	}
}
