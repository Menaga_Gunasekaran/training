/*
Requirements:
    - Create a class QueryStatement to store query statemants
    
Entities:
    - QueryStatement
    
Function Declaration:
    - 
    
Jobs to be done:
    - Create class QueryStatement
    - Declare all query statements as static variables
 
 
 * */

package com.kpr.training.constant;

public class QueryStatement {

	public static StringBuilder INSERT_ADDRESS = new StringBuilder("INSERT INTO Address(street, city, pincode)" +
		                                                           "      VALUES (?, ?, ?)");
	public static StringBuilder READ_ALL_ADDRESS = new StringBuilder("SELECT id, street, city, pincode" + 
		                                                             "      FROM ADDRESS");
	public static StringBuilder READ_ADDRESS = new StringBuilder("INSERT INTO Address(street, city, pincode)" + 
			                                                     "      VALUES (?, ?, ?)");
	public static StringBuilder UPDATE_ADDRESS = new StringBuilder("UPDATE Address" + 
				                                                   "      SET id = ?," + 
			                                                       "          street = ?" +
				                                                   "          city = ?," + 
			                                                  	   "          pincode = ?" + 
				                                                   "         WHERE id=?");
	public static StringBuilder DELETE_ADDRESS = new StringBuilder("DELETE FROM Address" + 
				                                                   "      WHERE id = ?; ");
	public static StringBuilder INSERT_PERSON = new StringBuilder("INSERT INTO Person(name, email, address_id, birth_date)" + 
				                                                  "      VALUES (?, ?, ?, ?);");
	public static StringBuilder READ_ALL_PERSON = new StringBuilder("SELECT id, name, email, address_id, birth_date, created_date " + 
				                                                    "      FROM Person");
	public static StringBuilder READ_PERSON = new StringBuilder("SELECT id, name, email, address_id, birth_date, created_date " + 
			                                                    "      FROM Person" + 
			                                                    "          WHERE id = ?;");
	public static StringBuilder UPDATE_PERSON = new StringBuilder("UPDATE Person" + 
			                                                      "    SET name = ? " + 
			                                                      "        ,email = ? " + 
			                                                      "        ,address_id = ? " + 
			                                                      "        ,birth_date = ? " + 
			                                                      "            WHERE id = ?;");
	public static StringBuilder DELETE_PERSON = new StringBuilder("DELETE FROM Person" + 
			                                                      "      WHERE id = ?;");
}
