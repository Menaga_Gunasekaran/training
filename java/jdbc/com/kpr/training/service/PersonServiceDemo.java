/*
Requirements:
    - CRUD OPeration for Person Class
     
Entities:
    - PersonService
    
Function Declaration:
    - public Person create(Person person)
    - public Person read(Long id, boolean includeAddress)
    - public List<Person> readAll()
    - public Person update(Person person)
    - public boolean delete(Long id)
    
Job to be done:

    1) Create class PersonService
    
    1) Declare create(Person person) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a storedPerson of type-Person with the values in parameter of create method
    5) Set the values in the preparedStatement with setLong(), setString() method
    6) Invoke executeUpdate() method and store the returned value in isInsert
    7) Invoke getGeneratedKeys() method to get the stored value
    8) Invoke getLong() method to get the id and store it in the id of storedPerson
    9) If Exception occurs, throw new AppException with custom messages
    10) Check if isInsert is equal to 1. If yes, print "Inserted Successfully"
        10.1) Else print "Insertion failed"
    11) Close the connection with close() method and return storedPerson
    
    
    1) Declare and define create(Person person, Address address)
    2) Create a storedAddress of type Address and storedPerson of type Person. Set their values to null.
    3) Invoke connect() method with SQL query  as argument for inserting person from SqlConnection class
    4) Get the returned PrepareStatement and store it in statement
    5) Create a AddressService object and invoke create(address) method and pass address as argument
    6) Get the returned value and store it in storedAddress
    7) Check whether storedAddress id is not equal to 0L
        7.1) Create a person and with the id of address and other values from person and store it to storedPerson
        7.2) Set the storedPerson values to prepared statements with setString(), setLong().
        7.3) Invoke executeUpdate() method to store the person to database and store the returned value in isUpdate
        7.4) Invoke getGeneratedKeys() to get the inserted value
        7.5) Invoke getLong(1) to get the id of stored value and store it to storedPerson with setId() method
        7.6) Add a catch block to catch the error 
            7.6.1) Invoke delete(Long id) method of AddrssService class with id of stordAddress as argument to delete the address.
                   Then throw new AppException with custom message
        7.7) Check whether isUpdate is equal to 1. If yes print "Updated Successfully"
            7.7.1) Else Invoke delete(Long id) method of AddrssService class with id of stordAddress as argument to delete the address.
    8) Else print message "Failed"
    9) Close the connection with close() method and return storedPerrson  
    
    
    1) Declare and define create(Person person, Address address)
    2) Create a storedAddress of type Address and storedPerson of type Person. Set their values to null. 
    3) Create a SqlConnection object, and statement of type PreparedStatement and set them to null
    4) Invoke formatCheck(address) method and set address as argument and check whether the returned boolean is true
    5) Invoke formatCheck(person) method and set person as argument and check whether the returned boolean is true
    6) Invoke connect() method with SQL query to insert address. Get the query argument for inserting address from SqlConnection class
    7) Get the returned PrepareStatement and store it in statement
    8) Invoke method connection.setAutoCommit(false)
    9) Store the values of address from parameter to storedAddress
    10) Set the values with setInt() and setString() and setLong() methods
    11) After setting values, Invoke executeUpdate() method to update to the database and store the returned integer in isInsert
        11.1) If any SQLException occurs, throw a new AppException with custom message
    12) Invoke getGeneratedKeys() method to get the result-set of inserted Address
    13) Invoke getLong() method to get the id of Address and store the id in storedAddress with setId(id) method
    14) Check whether isInsert is equal to 1 
    15) Invoke connect() method with SQL query for inserting person. Get the query argument from SqlConnection class
    16) Get the returned PrepareStatement and store it in statement
    17) Store the values in parameter from person to storedPerson
    18) Set the values in the preparedStatement with setLong(), setString() method
    19) Invoke executeUpdate() method and store the returned value in isInsert
    20) Invoke getGeneratedKeys() method to get the stored value
    21) Invoke getLong() method to get the id and store it in the id of storedPerson
    22) If Exception occurs, throw new AppException with custom messages
    23) Check if isInsert is equal to 1. If yes, print "Inserted Successfully"
        23.1) Else print "Insertion failed"
        23.2) Invoke con.rollback() method
    24) Invoke con.commit() method and close the connection by invoking close() method
    25) return storedPerson
             
          
    1) Declare read(Long id) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a storedPerson of type-Person and set it to null
    5) Create a object for AddressService class as - addressService
    6) Create a Address-object and set it to null
    7) Set the id of person to read to perparedStatement with setLong() method
    8) Invoke executeQuery() method and store the returned value in result of type- ResultSet
    9) Invoke next() method to move the position of result to next row
    10) Invoke values from result with getInt(), getString(), getLong() and create a new Person and store it to storedPerson
    11) Print the storedPerson values.
    12) With the address_id from storedPerson, Invoke read(Long id) method of address and print the address
    13) Add a catch block to catch SQLException and throw new AppException with custom message
    14) return storedPerson

    
    
    1) Declare readAll() method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a person of type-Person and set it to null
    5) Create a object for AddressService class as - addressService
    6) Create a address of type Address and set it to null
    7) Create a List of type Person as persons.
    8) Invoke executeQuery() method and store the returned value in result of type- ResultSet
    9) Create a while loop and invoke next() method in it.
        9.1) Get the values from result with getLong(), getString() method
        9.2) Create a person of type Person with the values from result and add the person to list-persons
        9.3) Invoke read(Long id) method with address_id of person as argument
        9.4) Store the returned value in address
        9.5) Print the value in person and address
    10) Add a catch block to catch SQLException and throw new AppException with custom message
    11) return persons-List
    
    
    1) Declare update(Person person) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Set the values to the prepredStatement with the values in the parameter
    5) Invoke executeUpdate() method to update and store the returned value in isUpdate
    6) Add a catch block to catch SQLException and throw new AppException
    7) Check whether isUpdate is equal to 1. If yes, print "Updated Successfully" method
        7.1) Else print "Updating Failed" message
    8) Close the connection and return the updated value of person


    1) Declare delete(Long id) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Set the id of the person to the prepredStatement from parameter-id with setLong() method
    5) Invoke executeUpdate() method and store the returned value in isDelete
    6) Check whether isDelete is equal to 1. 
        6.1) If yes, print "deleted Successfully"
        6.2) Close the connection with close() method
        6.3) return true
        6.4) Else print "Process failed"
        6.5) Close the connection with close() method
        .6) return false
    7) Close the connection with close() method  
    8) return true
    
    
Pseudo code:
public class PersonService {

    public Person create(Person person) {
    	SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonInsert());
		PreparedStatement statement = connector.statement;
		Person storedPerson = new Person(person.getName(), person.getEmail(), person.getAddressId(), person.getBirthDate());
		ResultSet result = null;
		int isUpdate = 0;
		try {
			if (connector.formatCheck(person) == true) {
				statement.setString(1, person.getName());
				statement.setString(2, person.getEmail());
				statement.setLong(3, person.getAddressId());
				statement.setString(4, person.getBirthDate());
				isUpdate = statement.executeUpdate();
				result = statement.getGeneratedKeys();
				storedPerson.setId(result.getLong(1));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		
		if (isUpdate == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Failed");
		}
		
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedPerson;
    }
    
    public Person create(Person person, Address address) {
		AddressService addressService = new AddressService();
		Address storedAddress = null;
		Person storedPerson = null;
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonInsert());
		PreparedStatement statement = connector.statement;
		int isUpdate = 0;
		if (connector.formatCheck(person) == true) {
			storedAddress = addressService.create(address);
		}
		if (storedAddress.getId() != 0L) {
			storedPerson = new Person(person.getName(), person.getEmail(), storedAddress.getId(), person.getBirthDate());
			try {
				statement.setString(1, storedPerson.getName());
				statement.setString(2, storedPerson.getEmail());
				statement.setLong(3, storedPerson.getAddressId());
				statement.setString(4, storedPerson.getBirthDate());
				isUpdate = statement.executeUpdate();
				ResultSet rs = statement.getGeneratedKeys();
				storedPerson.setId(rs.getLong(1));
			} catch (SQLException e) {
				System.out.println("Failed");
				addressService.delete(storedAddress.getId());
				throw new AppException(e.getMessage(), "3");
			}
			if (isUpdate != 0) {
				System.out.println("Updated Successfully");
			} else {
				System.out.println("Failed");
				addressService.delete(storedAddress.getId());
			}
		} else {
			System.out.println("Failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedPerson;
    }
    
    public Person read(Long id) {
    	SqlConnection connector = new SqlConnection();
    	connector = connector.connect(connector.getPersonRead());
    	PreparedStatement statement = connector.statement;
    	AddressService addressService = new AddressService();
    	Address address = null;
    	Person storedPerson = null;
    	ResultSet result = null;
		try {
			statement.setLong(1, id);
			result = statement.executeQuery();
			if (result.next()) {
				storedPerson = new Person(result.getLong(1),  result.getString(2), result.getString(3), result.getLong(4),  result.getString(5));
				address = addressService.read(storedPerson.getAddressId());
				System.out.print(storedPerson.getId() + " | " + storedPerson.getName() + " | " + storedPerson.getEmail() + " | " + storedPerson.getBirthDate() + " | "); 
				System.out.print(address.getStreet() + " | " + address.getCity() + " | " + address.getPincode());
				System.out.println(" | " + result.getString(5));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("No data found");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedPerson;
    }
    
    public List<Person> readAll() {
    	SqlConnection connector = new SqlConnection();
    	connector = connector.connect(connector.getPersonReadAll());
    	PreparedStatement statement = connector.statement;
    	ResultSet result = null;  
    	List<Person> persons = new ArrayList<>();
    	Person person = null;
    	Address address = null;
    	try {
    		AddressService addressService = new AddressService();
    		result = statement.executeQuery();
			while(result.next()) {
				person = new Person(result.getLong(1), result.getString(2), result.getString(3), result.getLong(4), result.getString(5));
				address = addressService.read(person.getAddressId());
				persons.add(person);
				System.out.print(person.getId() + " | " + person.getName() + " | " + person.getEmail() + " | " + person.getBirthDate() + " | "); 
				System.out.print(address.getStreet() + " | " + address.getCity() + " | " + address.getPincode());
				System.out.println(" | " + result.getString(5));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("No data found");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return persons;
    }
    
	public Person update(Person person) {
    	SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonUpdate());
		PreparedStatement statement = connector.statement;
		int isUpdate = 0;
		try {
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setString(4, person.getBirthDate());
			statement.setLong(5, person.getId());
			isUpdate = statement.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isUpdate == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return person;
    }
    
    public boolean delete(Long id) {
    	SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonDelete());
		PreparedStatement statement = connector.statement;
		int isDelete = 0;
		try {
			statement.setLong(1, id);
			isDelete = statement.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isDelete == 1) {
			System.out.println("Deleted Successfully");
		} else {
			System.out.println("Failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return true;
    }
}
 * */


package com.kpr.training.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.connection.SqlConnection;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;

public class PersonServiceDemo {

    public Person create(Person person) {
    	SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonInsert());
		PreparedStatement statement = connector.statement;
		Person storedPerson = new Person(person.getName(), person.getEmail(), person.getAddressId(), person.getBirthDate());
		ResultSet result = null;
		int isInsert = 0;
		try {
			if (connector.formatCheck(person) == true) {
				statement.setString(1, person.getName());
				statement.setString(2, person.getEmail());
				statement.setLong(3, person.getAddressId());
				statement.setString(4, person.getBirthDate());
				isInsert = statement.executeUpdate();
				result = statement.getGeneratedKeys();
				result.next();
				storedPerson.setId(result.getLong(1));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		
		if (isInsert == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Failed");
		}
		
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedPerson;
    }
    
    public Person create(Person person, Address address) {
		AddressServiceDemo addressService = new AddressServiceDemo();
		Address storedAddress = null;
		Person storedPerson = null;
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonInsert());
		PreparedStatement statement = connector.statement;
		int isInsert = 0;
		if (connector.formatCheck(person) == true) {
			storedAddress = addressService.create(address);
		}
		if (storedAddress.getId() != 0L) {
			storedPerson = new Person(person.getName(), person.getEmail(), storedAddress.getId(), person.getBirthDate());
			try {
				statement.setString(1, storedPerson.getName());
				statement.setString(2, storedPerson.getEmail());
				statement.setLong(3, storedPerson.getAddressId());
				statement.setString(4, storedPerson.getBirthDate());
				isInsert = statement.executeUpdate();
				ResultSet result = statement.getGeneratedKeys();
				result.next();
				storedPerson.setId(result.getLong(1));
			} catch (SQLException e) {
				System.out.println("Failed");
				addressService.delete(storedAddress.getId());
				throw new AppException(e.getMessage(), "3");
			}
			if (isInsert != 0) {
				System.out.println("Updated Successfully");
			} else {
				System.out.println("Failed");
				addressService.delete(storedAddress.getId());
			}
		} else {
			System.out.println("Failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedPerson;
    }
    
    public Person read(Long id, boolean includeAddress) {
    	SqlConnection connector = new SqlConnection();
    	connector = connector.connect(connector.getPersonRead());
    	PreparedStatement statement = connector.statement;
    	AddressServiceDemo addressService = new AddressServiceDemo();
    	Address address = null;
    	Person storedPerson = null;
    	ResultSet result = null;
		try {
			statement.setLong(1, id);
			result = statement.executeQuery();
			if (result.next()) {
				storedPerson = new Person(result.getLong(1),  result.getString(2), result.getString(3), result.getLong(4),  result.getString(5), result.getString(6));
				System.out.print(storedPerson.getId() + " | " + storedPerson.getName() + " | " + storedPerson.getEmail() + " | " + storedPerson.getBirthDate() + " | ");
				if (includeAddress == true) {
					address = addressService.read(storedPerson.getAddressId());
					System.out.print(" | " +address.getStreet() + " | " + address.getCity() + " | " + address.getPincode());
				}
				System.out.println(storedPerson.getCreatedDate());
			} else {
				System.out.println("No person found");
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("No data found");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedPerson;
    }
    
    public List<Person> readAll() {
    	SqlConnection connector = new SqlConnection();
    	connector = connector.connect(connector.getPersonReadAll());
    	PreparedStatement statement = connector.statement;
    	ResultSet result = null;  
    	List<Person> persons = new ArrayList<>();
    	Person person = null;
    	Address address = null;
    	try {
    		AddressServiceDemo addressService = new AddressServiceDemo();
    		result = statement.executeQuery();
			while(result.next()) {
				person = new Person(result.getLong(1), result.getString(2), result.getString(3), result.getLong(4), result.getString(5));
				address = addressService.read(person.getAddressId());
				persons.add(person);
				System.out.print(person.getId() + " | " + person.getName() + " | " + person.getEmail() + " | " + person.getBirthDate() + " | "); 
				System.out.print(address.getStreet() + " | " + address.getCity() + " | " + address.getPincode());
				System.out.println(" | " + result.getString(5));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("No data found");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return persons;
    }
    
	public Person update(Person person) {
    	SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonUpdate());
		PreparedStatement statement = connector.statement;
		int isUpdate = 0;
		try {
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setString(4, person.getBirthDate());
			statement.setLong(5, person.getId());
			isUpdate = statement.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isUpdate == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return person;
    }
    
    public boolean delete(Long id) {
    	SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getPersonDelete());
		PreparedStatement statement = connector.statement;
		int isDelete = 0;
		try {
			statement.setLong(1, id);
			isDelete = statement.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isDelete == 1) {
			try {
				statement.close();
				connector.connect.close();
			} catch (SQLException e) {
				throw new AppException(e.getMessage(), "3");
			}
			System.out.println("Deleted Successfully");
			return true;
		} else {
			try {
				statement.close();
				connector.connect.close();
			} catch (SQLException e) {
				throw new AppException(e.getMessage(), "3");
			}
			System.out.println("Deletion Failed");
			return false;
		}
    }
}
