/*
Requirements:
    - Define AddressService class for CRUD (create, read, readAll, update and delete) operations for Address using JDBC
  use PreparedStatement appropriately
 
Entities:
    - AddressService
    
Function Declaration: 
    - public Address create(Address address)
    - public Address read(Long id)
    - public List<Address> readAll()
    - public Address update(Address address)
    - public boolean delete(Long id)
    
Job to be done:
    1) Create class AddressService 
    
    1) Declare create(Address address) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a storedAddress of Address-Object with the values in parameter
    5) Invoke formatCheck(address) method and set address as argument and check whether the returned boolean is true
    6) Set the values with setInt() and setString() and setLong() methods
    7) After setting values, Invoke executeUpdate() method to update to the database and store the returned integer in isInsert
        7.1) If any SQLException occurs, throw a new AppException with custom message
    8) Invoke getGeneratedKeys() method to get the result-set of inserted Address
    9) Invoke getLong() method to get the id of Address and store the id in storedAddress with setId(id) method
    10) Check whether isInsert is equal to 1 and print "Inserted Successfully"
        10.1) Or throw new AppException as Insertion failed
    11) Close the connection with close() method
    12) Return the storedAddress.
    
    
    1) Declare read(Long id) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a address of Address-Object and set it to null
    5) Set the id value in statement by invoking setLong() method 
    6) Invoke executeQuery() method and store the returned value in result of type ResultSet
    7) Invoke next() method to move the pointer from first row
    8) Get the values by invoking getString(), getInt() method and store them in address
        8.1) If any Exception occurs, catch it and throw anew AppException with custom message
    9) Check if result is equal to null and throw error message
    10) Close the connection with close() method and return the address
    
    
    1) Declare readAll() method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a List-addresses of type Address-Object 
    6) Invoke executeQuery() method and store the returned value in result of type ResultSet
    7) Create a while loop and run next() method in it.
        7.1) Create a Address object with methods getString(), getInt() method and push it to the list-addresses of type Address
    8) If any Exception occurs, catch it and throw anew AppException with custom message
    9) Check whether result is equal to null, if yes print "Read Address operation failed"
    10) Close the connection with close() method
    11) return the list of address
    
   
    1) Declare update(Address address) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a integer isUpdate and set it to 0
    5) Invoke formatCheck(address) method and set address as argument and check whether the returned boolean is true
    6) Set the values with setInt() and setString() and setLong() methods
    7) After setting values, Invoke executeUpdate() method to update to the database and store the returned integer in isUpdate
        7.1) If any SQLException occurs, throw a new AppException with custom message
    8) Check if isUpdate is equal to 1. If yes, print "Updated Successfully"
        8.1) Or print "Updating process failed" message
    9) Close the connection with close() method
    10) Return the updated address
    
    1) Declare delete(Long id) method
    2) Invoke connect() method with SQL query as argument from SqlConnection class
    3) Get the returned PrepareStatement and store it in statement
    4) Create a integer isDelete and set it to 0
    5) Set the id of address to statement with setLong() method
    6) Invoke method executeQuery() to execute the query and store the returned values in isDelete
    7) Check whether isDelete is equal to 1, if yes Print "Deleted Successfully". Close the connection with close() method and return false.
        7.1) Or Print "Deletion Failed" message. Close the connection with close() method and return false
        
Pseudocode:
public class AddressService {
	
	public Address create(Address address) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressInsert());
		PreparedStatement statement = connector.statement;
		Address storedAddress = new Address(address.getStreet(), address.getCity(), address.getPincode());
		int isInsert = 0;
		try {
			if (connector.formatCheck(address) == true) {
				statement.setString(1, address.getStreet());
				statement.setString(2, address.getCity());
				statement.setInt(3, address.getPincode());
				isInsert = statement.executeUpdate();
				ResultSet result = statement.getGeneratedKeys();
				storedAddress.setId(result.getLong(1));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isInsert == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Failed");
			throw new AppException("Insertion Failed", "7");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedAddress;
	}
	
	public Address read(Long id) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressSingleReader());
		PreparedStatement statement = connector.statement;
		Address address = null;
		ResultSet result = null;
		try {
			statement.setLong(1, id);
			result = statement.executeQuery();
			if (result.next()) {
				address = new Address(result.getLong(1), result.getString(2), result.getString(3), result.getInt(4));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("No data found");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return address;
	}
	
	public List<Address> readAll() throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressReader());
		PreparedStatement statement = connector.statement;
		List<Address> addresses = new ArrayList<>();
		ResultSet result = null;
		try {
			result = statement.executeQuery();
			while(result.next()) {
				addresses.add(new Address(result.getLong(1), result.getString(2), result.getString(3), result.getInt(4)));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("Read Address operation failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return addresses;
	}
	
	public Address update(Address address) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressUpdate());
		PreparedStatement statement = connector.statement;
		int isUpdate = 0;
		try {
			if (connector.formatCheck(address) == true) {
				statement.setString(1, address.getStreet());
				statement.setString(2, address.getCity());
				statement.setInt(3, address.getPincode());
				statement.setLong(4, address.getId());
				isUpdate = statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isUpdate == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Updating process failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return address;
	}
	
	public boolean delete(Long id) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressDelete());
		PreparedStatement statement = connector.statement;
		int isDelete = 0;
		try {
			statement.setLong(1, id);
			isDelete = statement.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isDelete == 1) {
			System.out.println("Deleted Successfully");
			try {
				statement.close();
				connector.connect.close();
			} catch (SQLException e) {
				throw new AppException(e.getMessage(), "3");
			}
			return true;
		} else {
			System.out.println("Failed");
			try {
				statement.close();
				connector.connect.close();
			} catch (SQLException e) {
				throw new AppException(e.getMessage(), "3");
			}
			return false;
		}
	}
}
 */

package com.kpr.training.service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.connection.SqlConnection;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;

public class AddressServiceDemo {
	
	public Address create(Address address) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressInsert());
		PreparedStatement statement = connector.statement;
		Address storedAddress = new Address(address.getStreet(), address.getCity(), address.getPincode());
		int isInsert = 0;
		try {
			if (connector.formatCheck(address) == true) {
				statement.setString(1, address.getStreet());
				statement.setString(2, address.getCity());
				statement.setInt(3, address.getPincode());
				isInsert = statement.executeUpdate();
				ResultSet result = statement.getGeneratedKeys();
				result.next();
				storedAddress.setId(result.getLong(1));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isInsert == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Failed");
			throw new AppException("Insertion Failed", "7");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return storedAddress;
	}
	
	public Address read(Long id) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressSingleReader());
		PreparedStatement statement = connector.statement;
		Address address = null;
		ResultSet result = null;
		try {
			statement.setLong(1, id);
			result = statement.executeQuery();
			if (result.next()) {
				address = new Address(result.getLong(1), result.getString(2), result.getString(3), result.getInt(4));
			} else {
				System.out.println("No data found");
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("No data found");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return address;
	}
	
	public List<Address> readAll() throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressReader());
		PreparedStatement statement = connector.statement;
		List<Address> addresses = new ArrayList<>();
		ResultSet result = null;
		try {
			result = statement.executeQuery();
			while(result.next()) {
				addresses.add(new Address(result.getLong(1), result.getString(2), result.getString(3), result.getInt(4)));
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (result == null) {
			System.out.println("Read Address operation failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return addresses;
	}
	
	public Address update(Address address) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressUpdate());
		PreparedStatement statement = connector.statement;
		int isUpdate = 0;
		try {
			if (connector.formatCheck(address) == true) {
				statement.setString(1, address.getStreet());
				statement.setString(2, address.getCity());
				statement.setInt(3, address.getPincode());
				statement.setLong(4, address.getId());
				isUpdate = statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isUpdate == 1) {
			System.out.println("Updated Successfully");
		} else {
			System.out.println("Updating process failed");
		}
		try {
			statement.close();
			connector.connect.close();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		return address;
	}
	
	public boolean delete(Long id) throws AppException {
		SqlConnection connector = new SqlConnection();
		connector = connector.connect(connector.getAddressDelete());
		PreparedStatement statement = connector.statement;
		int isDelete = 0;
		try {
			statement.setLong(1, id);
			isDelete = statement.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(e.getMessage(), "3");
		}
		if (isDelete == 1) {
			System.out.println("Deleted Successfully");
			try {
				statement.close();
				connector.connect.close();
			} catch (SQLException e) {
				throw new AppException(e.getMessage(), "3");
			}
			return true;
		} else {
			System.out.println("Failed");
			try {
				statement.close();
				connector.connect.close();
			} catch (SQLException e) {
				throw new AppException(e.getMessage(), "3");
			}
			return false;
		}
	}
}
