CREATE TABLE `amazon`.`shopping` (
  `shop_id` INT(11) NOT NULL,
  `product_name` VARCHAR(45) NOT NULL,
  `amount` INT(11) NOT NULL,
  `delivery_date` DATE NULL,
  PRIMARY KEY (`shop_id`));
CREATE TABLE `amazon`.`order` (
  `delivery_date` DATE NOT NULL,
  `order_id` VARCHAR(45) NOT NULL,
  `payment_details` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`delivery_date`),
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC) VISIBLE);
ALTER TABLE `amazon`.`shopping` 
ADD INDEX `delivery_date_idx` (`delivery_date` ASC) VISIBLE;
;
ALTER TABLE `amazon`.`shopping` 
ADD CONSTRAINT `delivery_date`
  FOREIGN KEY (`delivery_date`)
  REFERENCES `amazon`.`order` (`delivery_date`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
