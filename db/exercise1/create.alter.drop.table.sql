CREATE SCHEMA `amazon` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
CREATE TABLE `amazon`.`shopping` (
  `product_id` INT NOT NULL,
  PRIMARY KEY (`product_id`));
ALTER TABLE `amazon`.`shopping` 
RENAME TO  `amazon`.`payment`;

DROP TABLE `amazon`.`payment`;
CREATE TABLE `amazon`.`shopping` (
  `shop_id` INT(11) NOT NULL,
  `product_name` VARCHAR(45) NOT NULL,
  `amount` INT(11) NOT NULL,
  `delivery_date` DATE NULL,
  PRIMARY KEY (`shop_id`));
