ALTER TABLE `amazon`.`shopping` 
ADD COLUMN `price` INT NOT NULL AFTER `product_name`,
ADD COLUMN `colour` VARCHAR(45) NOT NULL AFTER `price`,
CHANGE COLUMN `product_name` `product_name` VARCHAR(45) NOT NULL ;
INSERT INTO `amazon`.`shopping` (`id`, `product_name`, `price`, `colour`) VALUES ('1', 'dress', '1000', 'pink');
INSERT INTO `amazon`.`shopping` (`id`, `product_name`, `price`, `colour`) VALUES ('2', 'phone', '10000', 'white');
INSERT INTO `amazon`.`shopping` (`id`, `product_name`, `price`, `colour`) VALUES ('3', 'bag', '500', 'blue');
