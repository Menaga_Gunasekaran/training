CREATE TABLE `amazon`.`shopping` (
  `id` INT NOT NULL,
  PRIMARY KEY (`id`));
ALTER TABLE `amazon`.`shopping` 
ADD COLUMN `name` VARCHAR(45) NOT NULL AFTER `id`;
ALTER TABLE `amazon`.`shopping` 
CHANGE COLUMN `name` `product_name` VARCHAR(45) NOT NULL ;
DROP COLUMN `id`;