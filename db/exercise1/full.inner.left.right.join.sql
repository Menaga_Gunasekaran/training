SELECT shopping.product_name,payment.things
FROM  shopping FULL JOIN payment ON shopping.delivery_date = payment.delivery_date;

SELECT shopping.product_name,payment.things
FROM  shopping INNER JOIN payment ON shopping.delivery_date = payment.delivery_date;

SELECT shopping.product_name,payment.things
FROM  shopping LEFT JOIN payment ON shopping.delivery_date = payment.delivery_date;

SELECT shopping.product_name,payment.things
FROM  shopping RIGHT JOIN payment ON shopping.delivery_date = payment.delivery_date;