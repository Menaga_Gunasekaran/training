  SELECT student.`id`
         ,student.`roll_number`
	     ,student.`name`
         ,student.`gender`
         ,student.`phone` AS 'Student Contact'
         ,college.`name` AS 'College Name'
         ,semester_result.`semester`
         ,semester_result.`credits`
         ,semester_result.`grade`
   FROM student
         ,semester_result
         ,college
         ,university
   WHERE university.`univ_code` = college.`univ_code`
     AND college.`id` = student.`college_id_stu`
     AND student.`id` = semester_result.`stud_id_sem`
ORDER BY college.`name`, semester_result.`semester`
   LIMIT 10
  OFFSET 4;