 SELECT university.`university_name`
        ,college.`name`
        ,semester_fee.`semester`
        ,SUM(amount) AS 'collected_fees' 
        ,semester_fee.paid_year
   FROM semester_fee
        ,university
        ,student
        ,college
  WHERE semester_fee.stu_id = student.id
    AND student.college_id_stu = college.id
    AND college.univ_code = university.univ_code
    AND university_name = 'anna'
    AND paid_year = '2020'
    AND semester = '2'
    AND paid_status = 'notpaid';
 SELECT university.`university_name`
       ,SUM(amount) AS 'collected_fees' 
       ,semester_fee.paid_year
   FROM semester_fee
       ,university
       ,student
       ,college
  WHERE semester_fee.stu_id = student.id
    AND student.college_id_stu = college.id
    AND college.univ_code = university.univ_code
    AND university.university_name = 'anna'
    AND paid_status = 'paid'
    AND paid_year = '2020'