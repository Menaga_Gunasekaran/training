-- (a)
   USE `erdiagram`;
SELECT student.`id`
	   ,student.`roll_number`
       ,student.`name`
       ,student.`gender`
       ,college.`code`
       ,college.`name`
       ,semester_result.`grade`
       ,semester_result.`gpa`
  FROM university
       ,college
       ,student
       ,semester_result
 WHERE university.`univ_code` = college.`univ_code`
   AND student.`college_id_stu` = college.`id`
   AND semester_result.`stud_id_sem` = student.`id`
   AND semester_result.`gpa`>8;

-----------------------------------------------------------
-- (b)
SELECT student.`id`
	   ,student.`roll_number`
       ,student.`name`
       ,student.`gender`
       ,college.`code`
       ,college.`name`
       ,semester_result.`grade`
       ,semester_result.`gpa`
  FROM university
       ,college
       ,student
       ,semester_result
 WHERE university.`univ_code` = college.`univ_code`
   AND student.`college_id_stu` = college.`id`
   AND semester_result.`stud_id_sem` = student.`id`
   AND semester_result.`gpa`>5
   ORDER BY `gpa`;