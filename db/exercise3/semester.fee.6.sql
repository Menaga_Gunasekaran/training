  ALTER TABLE `erdiagram`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE(18,2) NOT NULL DEFAULT 40000.00 ,
CHANGE COLUMN `paid_year` `paid_year` YEAR(4) NULL DEFAULT NULL ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NOT NULL DEFAULT 'unpaid' ;
