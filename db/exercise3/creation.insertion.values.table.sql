CREATE SCHEMA erdiagram;
CREATE TABLE `erdiagram`.`college` (
  `id` INT NOT NULL,
  `code` CHAR(4) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `univ_code` CHAR(4) NULL,
  `city` VARCHAR(50) NOT NULL,
  `state` VARCHAR(50) NOT NULL,
  `year_opened` YEAR(4) NOT NULL,
  PRIMARY KEY (`id`, `state`),
  INDEX `univ_code_idx` (`univ_code` ASC) VISIBLE,
  CONSTRAINT `univ_code`
    FOREIGN KEY (`univ_code`)
    REFERENCES `erdiagram`.`university` (`univ_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE TABLE `erdiagram`.`department` (
  `dept_code` CHAR(4) NOT NULL,
  `dept_name` VARCHAR(50) NOT NULL,
  `univ_code_dept` CHAR(4) NOT NULL,
  PRIMARY KEY (`dept_code`),
  INDEX `univ_code_idx` (`univ_code_dept` ASC) VISIBLE,
  CONSTRAINT `univ_code_dept`
    FOREIGN KEY (`univ_code_dept`)
    REFERENCES `erdiagram`.`university` (`univ_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	CREATE TABLE `erdiagram`.`designation` (
  `id` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `rank` CHAR(1) NOT NULL,
  PRIMARY KEY (`id`));
CREATE TABLE `erdiagram`.`college_department` (
  `cdept_id` INT NOT NULL,
  `udept_code` CHAR(4) NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`cdept_id`),
  INDEX `udept_code_idx` (`udept_code` ASC) VISIBLE,
  INDEX `college_id_idx` (`college_id` ASC) VISIBLE,
  CONSTRAINT `udept_code`
    FOREIGN KEY (`udept_code`)
    REFERENCES `erdiagram`.`department` (`dept_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `college_id`
    FOREIGN KEY (`college_id`)
    REFERENCES `erdiagram`.`college` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE TABLE `erdiagram`.`employee` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `college_id_emp` INT NOT NULL,
  `cdept_id` INT NOT NULL,
  `desig_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE,
  INDEX `college_id_idx` (`college_id_emp` ASC) VISIBLE,
  INDEX `desig_id_idx` (`desig_id` ASC) VISIBLE,
  CONSTRAINT `college_id_emp`
    FOREIGN KEY (`college_id_emp`)
    REFERENCES `erdiagram`.`college` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cdept_id`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `erdiagram`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `desig_id`
    FOREIGN KEY (`desig_id`)
    REFERENCES `erdiagram`.`designation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE TABLE `erdiagram`.`syllabus` (
  `id` INT NOT NULL,
  `cdept_id_syll` INT NOT NULL,
  `syllabus_code` CHAR(4) NOT NULL,
  `syllabus_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `cdept_id_syll_idx` (`cdept_id_syll` ASC) VISIBLE,
  CONSTRAINT `cdept_id_syll`
    FOREIGN KEY (`cdept_id_syll`)
    REFERENCES `erdiagram`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	CREATE TABLE `erdiagram`.`proffessor_syllabus` (
  `emp_id` INT NOT NULL,
  `syllabus_id_syll` INT NOT NULL,
  `semester` TINYINT NOT NULL,
  INDEX `emp_id_idx` (`emp_id` ASC) VISIBLE,
  INDEX `syllabus_id_syll_idx` (`syllabus_id_syll` ASC) VISIBLE,
  CONSTRAINT `emp_id`
    FOREIGN KEY (`emp_id`)
    REFERENCES `erdiagram`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `syllabus_id_syll`
    FOREIGN KEY (`syllabus_id_syll`)
    REFERENCES `erdiagram`.`syllabus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	CREATE TABLE `erdiagram`.`student` (
  `id` INT NOT NULL,
  `roll_number` CHAR(8) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `academic_year` YEAR(4) NOT NULL,
  `cdept_id_stu` INT NOT NULL,
  `college_id_stu` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `cdept_id_stu_idx` (`cdept_id_stu` ASC) VISIBLE,
  INDEX `college_id_stu_idx` (`college_id_stu` ASC) VISIBLE,
  CONSTRAINT `cdept_id_stu`
    FOREIGN KEY (`cdept_id_stu`)
    REFERENCES `erdiagram`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `college_id_stu`
    FOREIGN KEY (`college_id_stu`)
    REFERENCES `erdiagram`.`college` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE TABLE `erdiagram`.`semester_fee` (
  `cdept_id_sem` INT NOT NULL,
  `stu_id` INT NOT NULL,
  `semester` TINYINT NOT NULL,
  `amount` DOUBLE(18,2) NOT NULL,
  `paid_year` YEAR(4) NOT NULL,
  `paid_status` VARCHAR(10) NOT NULL,
  INDEX `cdept_id_sem_idx` (`cdept_id_sem` ASC) VISIBLE,
  INDEX `stu_id_idx` (`stu_id` ASC) VISIBLE,
  CONSTRAINT `cdept_id_sem`
    FOREIGN KEY (`cdept_id_sem`)
    REFERENCES `erdiagram`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `stu_id`
    FOREIGN KEY (`stu_id`)
    REFERENCES `erdiagram`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	CREATE TABLE `erdiagram`.`semester_result` (
  `stud_id_sem` INT NOT NULL,
  `syllabus_id` INT(11) NOT NULL,
  `semester` TINYINT NOT NULL,
  `grade` VARCHAR(2) NOT NULL,
  `credits` FLOAT NOT NULL,
  `result_date` DATE NOT NULL,
  INDEX `stud_id_sem_idx` (`stud_id_sem` ASC) VISIBLE,
  INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE,
  CONSTRAINT `stud_id_sem`
    FOREIGN KEY (`stud_id_sem`)
    REFERENCES `erdiagram`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `syllabus_id`
    FOREIGN KEY (`syllabus_id`)
    REFERENCES `erdiagram`.`syllabus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
