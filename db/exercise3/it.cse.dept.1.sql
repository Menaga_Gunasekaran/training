 SELECT college.code
      ,college.name AS college_name
	  ,university.university_name
	  ,college.city
	  ,college.state
	  ,college.year_opened
	  ,department.dept_name AS dept_name
	  ,employee.name AS hod_name 
  FROM college
	  ,university
	  ,department
	  ,employee
	  ,designation
	  ,college_department 
 WHERE university.univ_code=college.univ_code
   AND employee.college_id_emp=college.id
   AND college_department.cdept_id=employee.cdept_id
   AND designation.id=employee.desig_id
   AND department.dept_code=college_department.udept_code
   AND designation.name="hod"
HAVING  department.dept_name="it" 