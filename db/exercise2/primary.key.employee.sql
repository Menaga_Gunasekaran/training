ALTER TABLE `training_exp1`.`employee` 
ADD CONSTRAINT `department_id`
  FOREIGN KEY (`department_id`)
  REFERENCES `training_exp1`.`department` (`department_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;