ALTER TABLE `training_exp1`.`employee` 
DROP FOREIGN KEY `dept_id`;
ALTER TABLE `training_exp1`.`employee` 
CHANGE COLUMN `dept_id` `dept_id` VARCHAR(7) NULL DEFAULT NULL ;
ALTER TABLE `training_exp1`.`employee` 
ADD CONSTRAINT `dept_id`
  FOREIGN KEY (`dept_id`)
  REFERENCES `training_exp1`.`department` (`department_id`);
  
SELECT first_name,surname FROM employee WHERE dept_id = 'NULL';