CREATE SCHEMA `training_exp1 ;

CREATE TABLE `training_exp1`.`employee` (
  `employee_id` VARCHAR(7) NOT NULL
  ,`first_name` VARCHAR(8) NOT NULL
  ,`surname` VARCHAR(8) NOT NULL
  ,`dob` DATE NOT NULL
  ,`date_of_joining` DATE NOT NULL
  ,`annual_salary` INT(5) NOT NULL
  ,`deparment_id` VARCHAR(7) NOT NULL
  PRIMARY KEY (`employee_id`)
  ,UNIQUE INDEX `employee_id_UNIQUE` (`employee_id` ASC) VISIBLE);

CREATE TABLE `employee`.`department` (
  `department_id` VARCHAR(7) NOT NULL
  ,`department_name` VARCHAR(15) NOT NULL
  PRIMARY KEY (`department_id`));
  ALTER TABLE `training_exp1`.`employee` 
ADD INDEX `department_id_idx` (`department_id` ASC) VISIBLE;
;
ALTER TABLE `training_exp1`.`employee` 
ADD CONSTRAINT `department_id`
  FOREIGN KEY (`department_id`)
  REFERENCES `training_exp1`.`department` (`department_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;